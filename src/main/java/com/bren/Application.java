package com.bren;

import java.util.Scanner;

/**
 * @author Andriy Bren
 * @version 1.1
 */
public class Application {
    private static Scanner sc = new Scanner(System.in);

    /**
     * This is the main method which makes use of printOddNumbersApp,
     * printFibonacciApp, printMenu methods. User have 3 option to choose.
     */
    public static void main(String[] args) {
        while (true) {
            printMenu();
            char choice = sc.next().charAt(0);
            switch (choice) {
                case 'q':
                    System.out.println("Good Bye!");
                    return;
                case '1':
                    printOddNumbersApp();
                    break;
                case '2':
                    printFibonacciApp();
                    break;
                default:
                    System.out.println("Choose correct option!!!");
                    break;
            }
        }
    }

    /**
     * This method prints menu of the program.
     */
    private static void printMenu() {
        System.out.println();
        System.out.println("===Second Task===");
        System.out.println("1 - Odd numbers");
        System.out.println("2 - Build Fibonacci numbers");
        System.out.println("q - Quit");
        System.out.println("Select: ");
        System.out.println();
    }

    /**
     * This method prints menu of the OddNumbersApp.
     * Method use checkInputData, getOddNumInOrder and
     * getOddNumInReverse methods
     */
    private static void printOddNumbersApp() {
        System.out.println("===Enter the interval===");
        System.out.println("Please enter the start number: ");
        int start = sc.nextInt();
        System.out.println("Please enter the end number: ");
        int end = sc.nextInt();
        if (checkInputData(start, end)) {
            System.out.println("ERROR!!!Input only positive.Input End > Start");
            return;
        }
        System.out.println("Odd numbers: ");
        getOddNumInOrder(start, end);
        getOddNumInReverse(start, end);
    }

    /**
     * This method prints odd number in order.
     * @param start initial value of interval which enter user
     * @param end final value of interval which which enter user
     */
    private static void getOddNumInOrder(int start, int end) {
        System.out.println("In order: ");

        while (start <= end) {
            if (start % 2 == 1) {
                System.out.print(start + " ");
            }
            start++;
        }
        System.out.println();
    }

    /**
     * This method prints odd number in reverse, prints sum of odd numbers
     * and sum of even numbers.
     * @param start initial value of interval which enter user
     * @param end final value of interval which which enter user
     */
    private static void getOddNumInReverse(int start, int end) {
        System.out.println();
        System.out.println("In reverse: ");
        int sumOfOddNumbers = 0;
        int sumOfEvenNumbers = 0;
        while (end >= start) {
            if (end % 2 == 1) {
                System.out.print(end + " ");
                sumOfOddNumbers += end;
            }
            if (end % 2 == 0) {
                sumOfEvenNumbers += end;
            }
            end--;
        }
        System.out.println();
        System.out.println("Sum of odd nubmers: " + sumOfOddNumbers);
        System.out.println("Sum of even nubmers: " + sumOfEvenNumbers);
    }

    /**
     * This method check correctness of input data.
     * @param start initial value of interval which enter user
     * @param end final value of interval which which enter user
     * @return boolean value
     */
    private static boolean checkInputData(final int start, final int end) {
        if ((start < 0) || (end < 0)) {
            return true;
        }
        return (end - start) < 0;
    }

    /**
     * This method prints menu of the FibonacciApp.
     * Method use getTheBiggestEvenAndOddNum,getPercentageOfEvenAndOddNum,
     * getSequenceFibonacci methods.
     */
    private static void printFibonacciApp() {
        System.out.println("===Fibonacci===");
        System.out.println("Enter size of sequence Fibonacci: ");
        int sizeOfSequence = sc.nextInt();
        if (sizeOfSequence < 2) {
            System.out.println("ERROR! Input size of sequence > 1");
            return;
        }
        System.out.println();
        getTheBiggestEvenAndOddNum(getSequenceFibonacci(sizeOfSequence));
        System.out.println();
        getPercentageOfEvenAndOddNum(getSequenceFibonacci(sizeOfSequence));
    }

    /**
     * This method build Fibonacci numbers with given size of Sequence by User.
     * @param sizeOfSequence size of Fibonacci sequence
     * @return array Fibonacci sequence
     */
    private static int[] getSequenceFibonacci(final int sizeOfSequence) {
        int[] array = new int[sizeOfSequence];
        array[0] = 1;
        array[1] = 1;
        System.out.print(array[0] + " " + array[1] + " ");
        for (int i = 2; i < sizeOfSequence; i++) {
            array[i] = array[i - 2] + array[i - 1];
            System.out.print(array[i] + " ");
        }
        return array;
    }

    /**
     * This method print the biggest even and odd number.
     * @param array Fibonacci sequence
     */
    private static void getTheBiggestEvenAndOddNum(final int[] array) {
        /* the biggest even number */
        int f1 = 0;

        /* the biggest odd number */
        int f2 = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i - 1] < array[i]) {
                if (array[i] % 2 == 0) {
                    f1 = array[i];
                }
                if (array[i] % 2 == 1) {
                    f2 = array[i];
                }
            }
        }
        System.out.println();
        System.out.println("The biggest even number: " + f1);
        System.out.println("The biggest odd number: " + f2);
    }

    /**
     * This method count number of even and odd numbers.
     * Method print percentage of even and odd numbers.
     * @param array Fibonacci sequence
     */
    private static void getPercentageOfEvenAndOddNum(final int[] array) {
        final int allPercentages = 100;
        int numberOfEven = 0;
        int numberOfOdd = 0;
        int percentOfEvenNum;
        int percentOfOddNum;
        for (int value : array) {
            if (value % 2 == 0) {
                numberOfEven++;
            }
            if (value % 2 == 1) {
                numberOfOdd++;
            }
        }
        System.out.println();
        System.out.println("Number of even: " + numberOfEven);
        System.out.println("Number of odd: " + numberOfOdd);
        percentOfEvenNum = (numberOfEven * allPercentages) / array.length;
        percentOfOddNum = (numberOfOdd * allPercentages) / array.length;
        System.out.println();
        System.out.println("Percent of even num: " + percentOfEvenNum + "%");
        System.out.println("Percent of odd num: " + percentOfOddNum + "%");
    }
}
